import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class babysteps(Node):
    
    def __init__(self):
        super().__init__("babytalks")
        self.publishers_ = self.create_publisher(String, 'babytalks', 10)
        self.timer = self.create_timer(timer_period_sec=0.5, callback=self.timer_callback)
        self.i = 0
    
    def timer_callback(self):
        msg = String()
        msg.data = 'Hello world %d' % self.i
        self.publishers_.publish(msg)
        self.get_logger().info('Publishing: "%s"'% msg.data)
        self.i += 1

def main(args=None):
    rclpy.init(args=args)

    minimal_pub = babysteps()
    rclpy.spin(minimal_pub)

    minimal_pub.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
